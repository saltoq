#include <QtCore/QJsonArray>
#include <CommHistory/ConversationModel>
#include "contactsmanager.h"
#include "commmanager.h"

using namespace CommHistory;

static const int RECORD_LIMIT = 20;
static const int PER_GROUP_LIMIT = 10;

CommManager::CommManager(Settings *settings, StorageManager *storage, ContactsManager *contacts, ToqManager *toq) :
	QObject(toq), _toq(toq), _contacts(contacts), _storage(storage), _settings(settings),
	_calls(new CallModel(this)),
	_convs(new GroupModel(this)),
	_refreshTimer(new QTimer(this))
{
	_calls->setQueryMode(EventModel::AsyncQuery);
	_calls->setSorting(CallModel::SortByTime);
	_calls->setResolveContacts(EventModel::ResolveImmediately);
	_calls->setLimit(RECORD_LIMIT);

	_convs->setQueryMode(EventModel::AsyncQuery);
	_convs->setLimit(RECORD_LIMIT);

	_refreshTimer->setSingleShot(true);
	_refreshTimer->setInterval(2000);

	connect(_calls, SIGNAL(modelReady(bool)),
			this, SLOT(scheduleRefresh()));
	connect(_calls, SIGNAL(modelReset()),
			this, SLOT(scheduleRefresh()));
	connect(_calls, SIGNAL(rowsInserted(QModelIndex,int,int)),
			this, SLOT(scheduleRefresh()));
	connect(_calls, SIGNAL(rowsRemoved(QModelIndex,int,int)),
			this, SLOT(scheduleRefresh()));

	connect(_convs, SIGNAL(modelReady(bool)),
			this, SLOT(scheduleRefresh()));
	connect(_convs, SIGNAL(modelReset()),
			this, SLOT(scheduleRefresh()));
	connect(_convs, SIGNAL(rowsInserted(QModelIndex,int,int)),
			this, SLOT(scheduleRefresh()));
	connect(_convs, SIGNAL(rowsRemoved(QModelIndex,int,int)),
			this, SLOT(scheduleRefresh()));

	connect(_refreshTimer, &QTimer::timeout,
			this, &CommManager::refresh);

	if (!_calls->getEvents()) {
		qWarning() << "Could not get the call log";
	}
	if (!_convs->getGroups()) {
		qWarning() << "Could not get conversation groups";
	}
}

void CommManager::scheduleRefresh()
{
	if (!_refreshTimer->isActive()) {
		_refreshTimer->start();
	}
}

void CommManager::refresh()
{
	qDebug() << "Refreshing recent comms";
	QMultiMap<QDateTime, QJsonObject> events;

	int rows = _calls->rowCount();
	for (int i = 0; i < rows; i++) {
		QModelIndex index = _calls->index(i, 0);
		Event e = _calls->event(index);
		const RecipientList &recipients = e.recipients();

		if (recipients.isEmpty()) {
			qCritical() << "Empty recipient list while reading event:" << e.toString();
		}

		const Recipient &recipient = recipients.first();

		QString name = recipient.contactName();
		if (name.isEmpty()) {
			name = recipient.contactId();
		}
		if (name.isEmpty()) {
			// TODO Check if this is still necessary
			name = tr("Private number");
		}

		QDateTime dt = e.startTime();
		QJsonObject obj;
		obj.insert("Name", name);
		obj.insert("ContactId", _contacts->getRecordIdForContact(recipient.contactId()));

		QJsonArray records;
		if (_calls->hasChildren(index)) {
			const int subrows = _calls->rowCount(index);
			for (int j = 0; j < subrows; j++) {
				QModelIndex index2 = _calls->index(j, 0, index);
				const Event e = _calls->event(index2);
				QJsonObject record;
				record.insert("CommsType", QLatin1String("Call"));
				record.insert("ReceivedTime", qint64(e.startTime().toTime_t()));
				record.insert("CallerId", recipient.remoteUid());
				record.insert("ItemId", e.id());
				QJsonObject details;
				details.insert("Duration", e.startTime().secsTo(e.endTime()));
				details.insert("PhoneType", QLatin1String("Other")); // TODO
				switch (e.direction()) {
				case Event::Inbound:
					details.insert("Direction", QLatin1String("Incoming"));
					break;
				case Event::Outbound:
					details.insert("Direction", QLatin1String("Outgoing"));
					break;
				default:
					details.insert("Direction", QLatin1String("Unknown"));
					break;
				}
				details.insert("IsMissedCall", e.isMissedCall());
				record.insert("CommsDetails", details);
				records.append(record);
			}
		}

		obj.insert("CommsRecords", records);

		events.insert(dt, obj);
	}

	QScopedPointer<ConversationModel> conv(new ConversationModel);
	conv->setTreeMode(false);
	conv->setLimit(PER_GROUP_LIMIT);
	conv->setQueryMode(EventModel::SyncQuery);

	rows = _convs->rowCount();
	for (int i = 0; i < rows; i++) {
		Group g = _convs->group(_convs->index(i, 0));

		const RecipientList &recipients = g.recipients();

		if (recipients.isEmpty()) {
			qCritical() << "Empty recipient list while reading event:" << g.toString();
		}

		const Recipient &recipient = recipients.first();
		QString name = recipient.contactName();
		if (name.isEmpty()) {
			name = recipient.contactId();
		}
		if (name.isEmpty()) {
			// TODO Check if this is still necessary
			name = tr("Unknown contact");
		}

		QJsonObject obj;
		obj.insert("Name", name);
		obj.insert("ContactId", _contacts->getRecordIdForContact(recipient.contactId()));

		QJsonArray records;

		if (conv->getEvents(g.id())) {
			const int subrows = conv->rowCount();
			for (int j = 0; j < subrows; j++) {
				Event e = conv->event(j);
				QJsonObject record;
				record.insert("CommsType", QLatin1String("Text"));
				record.insert("ReceivedTime", qint64(e.startTime().toTime_t()));
				record.insert("CallerId", recipient.remoteUid());
				record.insert("ItemId", e.id());
				QJsonObject details;
				details.insert("Message", e.freeText());
				details.insert("privileged", int(0)); // TODO
				switch (e.direction()) {
				case Event::Inbound:
					details.insert("Direction", int(1));
					break;
				case Event::Outbound:
					details.insert("Direction", int(2));
					break;
				default:
					details.insert("Direction", int(0));
					break;
				}
				details.insert("IsRead", e.isRead());
				record.insert("CommsDetails", details);
				records.append(record);
			}
		} else {
			qWarning() << "Failed to get events for group id" << g.id();
			QJsonObject record;
			record.insert("CommsType", QLatin1String("Text"));
			record.insert("ReceivedTime", qint64(g.startTime().toTime_t()));
			record.insert("CallerId", recipient.remoteUid());
			record.insert("ItemId", g.id());
			QJsonObject details;
			details.insert("Message", g.lastMessageText());
			details.insert("privileged", int(0)); // TODO
			details.insert("Direction", int(0));
			details.insert("IsRead", g.unreadMessages() == 0);
			record.insert("CommsDetails", details);
			records.append(record);
		}

		obj.insert("CommsRecords", records);

		events.insert(g.endTime(), obj);
	}

	QJsonArray records;

	int i = 0;
	auto it = events.end();
	while (it != events.begin()) {
		--it;

		QJsonObject record;
		record.insert("RecordId", i);
		record.insert("RecordPayload", it.value());
		records.append(record);

		if (++i >= RECORD_LIMIT) break;
	}

	QString storeName("Phub.Phone.RecentComms");
	QJsonObject store;
	store.insert("Name", storeName);
	store.insert("Records", records);

	QJsonObject root;
	root.insert("DataStore", store);

	qDebug() << "Got" << records.size() << "records";

	_storage->updateStore(storeName, root);
}
