#ifndef AGENDAMANAGER_H
#define AGENDAMANAGER_H

#include "storagemanager.h"
#include <extendedstorage.h>

class AgendaManager : public QObject, public mKCal::ExtendedStorageObserver
{
	Q_OBJECT
public:
	explicit AgendaManager(StorageManager *storage, ToqManager *toq);
	~AgendaManager();

public slots:
	void scheduleRefresh();

protected:
	void storageModified(mKCal::ExtendedStorage *storage, const QString &info) Q_DECL_OVERRIDE;
	void storageProgress(mKCal::ExtendedStorage *storage, const QString &info) Q_DECL_OVERRIDE;
	void storageFinished(mKCal::ExtendedStorage *storage, bool error, const QString &info) Q_DECL_OVERRIDE;

private slots:
	void refresh();

private:
	ToqManager *_toq;
	StorageManager *_storage;

	mKCal::ExtendedCalendar::Ptr _calendar;
	mKCal::ExtendedStorage::Ptr _calendarStorage;

	QTimer *_refreshTimer;
};

#endif // AGENDAMANAGER_H
