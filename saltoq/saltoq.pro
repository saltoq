# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = saltoq

CONFIG += sailfishapp

SOURCES += src/saltoq.cpp

OTHER_FILES += qml/saltoq.qml \
    qml/cover/CoverPage.qml \
    qml/pages/FirstPage.qml \
    qml/pages/SecondPage.qml \
    rpm/saltoq.changes.in \
    rpm/saltoq.spec \
    rpm/saltoq.yaml \
    translations/*.ts \
    saltoq.desktop

CONFIG += sailfishapp_i18n

#TRANSLATIONS += translations/saltoq-de.ts
