#ifndef TOQCONNECTION_H
#define TOQCONNECTION_H

#include <QtCore/QTimer>
#include <QtBluetooth/QBluetoothSocket>
#include <QtBluetooth/QBluetoothServer>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>

class ToqConnection : public QObject
{
	Q_OBJECT
	Q_ENUMS(CoreEndpoints)
	Q_PROPERTY(bool connected READ isConnected NOTIFY connectedChanged)
	Q_PROPERTY(QBluetoothAddress address READ address WRITE setAddress)

public:
	explicit ToqConnection(QObject *parent = 0);

	typedef quint8 Endpoint;

	enum CoreEndpoints
	{
		VersionEndpoint = 0,
		VoiceCallEndpoint = 1,
		SMSEndpoint = 3,
		SystemEndpoint = 5,
		PopUpEndpoint = 7,
		StorageServiceEndpoint = 9,
		TFTPEndpoint = 15,
		FMSEndpoint = 17,
		EPCommunicationEndpoint = 19,
		MusicEndpoint = 24,
		AppMessagingEndpoint = 26,
		SpeechEndpoint = 28,
		ActivityMonitoringEndpoint = 30,
		FTSEndpoint = 32,
		AppLoggingEndpoint = 34
	};

	struct Message
	{
		Message();
		Message(Endpoint source, Endpoint destination, quint16 transactionId, quint32 type, const QByteArray &payload);
		Message(Endpoint source, Endpoint destination, quint16 transactionId, quint32 type, const QJsonDocument &payload);

		Endpoint source;
		Endpoint destination;
		quint16 transactionId;
		quint32 type;
		QByteArray payload;

		QJsonDocument toJson() const;
	};

	static QString nameOfEndpoint(Endpoint ep);

	static quint32 checksum(const QByteArray &data);
	static quint32 checksum(QIODevice *dev);

	bool isConnected() const;
	QBluetoothAddress address() const;
	void setAddress(const QBluetoothAddress &address);

	quint16 newTransactionId();

public slots:
	void sendMessage(const Message &msg);
	void disconnectFromDevice();

signals:
	void connected();
	void disconnected();
	void messageReceived(const Message &msg);
	void connectedChanged();

private:
	static Message unpackMessage(const QByteArray &data);
	static QByteArray packMessage(const Message &msg);
	void setSocket(QBluetoothSocket *socket);

private slots:
	void tryConnect();
	void handleServerConnection();
	void handleSocketConnected();
	void handleSocketDisconnected();
	void handleSocketError(QBluetoothSocket::SocketError error);
	void handleSocketData();

private:
	QBluetoothAddress _address;
	QBluetoothServer *_server;
	QBluetoothSocket *_socket;
	QTimer *_reconnectTimer;
	quint16 _lastTransactionId;
};

inline ToqConnection::Message::Message()
{
}

inline ToqConnection::Message::Message(Endpoint source, Endpoint destination, quint16 transactionId, quint32 type, const QByteArray &payload)
	: source(source), destination(destination), transactionId(transactionId), type(type), payload(payload)
{
}

inline bool ToqConnection::isConnected() const
{
	return _socket && _socket->state() == QBluetoothSocket::ConnectedState;
}

inline QBluetoothAddress ToqConnection::address() const
{
	return _address;
}

#endif // TOQCONNECTION_H
