#ifndef VERSIONMANAGER_H
#define VERSIONMANAGER_H

#include "toqmanager.h"

class VersionManager : public QObject, public ToqManager::EndpointHandler
{
	Q_OBJECT
public:
	explicit VersionManager(ToqManager *toq);

	void handleMessage(const ToqConnection::Message &msg) Q_DECL_OVERRIDE;

private:
	void handleVersionMessage(const ToqConnection::Message &msg);

private:
	ToqManager *_toq;
};

#endif // VERSIONMANAGER_H
