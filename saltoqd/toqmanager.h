#ifndef TOQMANAGER_H
#define TOQMANAGER_H

#include <QtCore/QObject>

#include "toqconnection.h"
#include "settings.h"

class ObexConnection;
class VersionManager;
class SystemManager;
class FmsManager;
class StorageManager;
class SettingsManager;
class MusicManager;
class ContactsManager;
class CommManager;
class AgendaManager;
class VoiceCallManager;
class WeatherManager;
class CardManager;
class NotificationManager;

class ToqManager : public QObject
{
	Q_OBJECT
	Q_PROPERTY(bool connected READ isConnected NOTIFY connectedChanged)

	friend class ToqManagerAdaptor;

public:
	explicit ToqManager(Settings *settings, QObject *parent = 0);

	struct EndpointHandler {
		virtual void handleMessage(const ToqConnection::Message &msg) = 0;
	};

	void setEndpointListener(ToqConnection::Endpoint ep, EndpointHandler *handler);

	bool isConnected() const;

	quint16 newTransactionId();

	void sendMessage(const ToqConnection::Message &msg);
	void sendMessage(ToqConnection::Endpoint source, ToqConnection::Endpoint destination,
					 quint16 transactionId, quint32 type, const QByteArray &payload);
	quint16 sendMessage(ToqConnection::Endpoint source, ToqConnection::Endpoint destination,
					 quint32 type, const QByteArray &payload);
	void sendMessage(ToqConnection::Endpoint source, ToqConnection::Endpoint destination,
					 quint16 transactionId, quint32 type, const QJsonObject &payload);
	quint16 sendMessage(ToqConnection::Endpoint source, ToqConnection::Endpoint destination,
					 quint32 type, const QJsonObject &payload);
	void sendReply(const ToqConnection::Message &msg, quint32 type, const QJsonObject &payload);

signals:
	void connected();
	void disconnected();
	void connectedChanged();

private slots:
	void handleToqMessage(const ToqConnection::Message &msg);
	void handleSettingsAddressChanged();

private:
	Settings *_settings;
	ToqConnection *_conn;
	ObexConnection *_obex;
	QHash<ToqConnection::Endpoint, EndpointHandler*> _handlers;

	VersionManager *_versionManager;
	SystemManager *_systemManager;
	FmsManager *_fmsManager;
	StorageManager *_storageManager;
	SettingsManager *_settingsManager;
	MusicManager *_musicManager;
	ContactsManager *_contactsManager;
	CommManager *_commManager;
	AgendaManager *_agendaManager;
	VoiceCallManager *_voiceCallManager;
	WeatherManager *_weatherManager;
	CardManager *_cardManager;
	NotificationManager *_notificationManager;
};

inline bool ToqManager::isConnected() const
{
	return _conn->isConnected();
}

inline quint16 ToqManager::newTransactionId()
{
	return _conn->newTransactionId();
}

#endif // TOQMANAGER_H
