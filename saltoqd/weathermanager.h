#ifndef WEATHERMANAGER_H
#define WEATHERMANAGER_H

#include <QtCore/QFileSystemWatcher>
#include <QtGui/QImage>
#include "toqmanager.h"

class WeatherManager : public QObject
{
	Q_OBJECT
public:
	explicit WeatherManager(FmsManager *fms, ToqManager *toq);

private:
	QImage constructImage(const QJsonObject &obj);

private slots:
	void doRefresh();

private:
	ToqManager *_toq;
	FmsManager *_fms;
	QString _file;
	QFileSystemWatcher *_watcher;
	QTimer *_refreshTimer;
};

#endif // WEATHERMANAGER_H
