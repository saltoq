#include "versionmanager.h"

VersionManager::VersionManager(ToqManager *toq) :
	QObject(toq), _toq(toq)
{
	_toq->setEndpointListener(ToqConnection::VersionEndpoint, this);
}

void VersionManager::handleMessage(const ToqConnection::Message &msg)
{
	Q_ASSERT(msg.destination == ToqConnection::VersionEndpoint);
	switch (msg.type) {
	case 0:
		handleVersionMessage(msg);
		break;
	default:
		qWarning() << "Unknown version message" << msg.type;
		break;
	}
}

void VersionManager::handleVersionMessage(const ToqConnection::Message &msg)
{
	QJsonObject root = msg.toJson().object();
	qDebug() << "Remote AlohaVersion: " << root["AlohaVersion"].toString();

	QJsonObject reply;
	reply.insert("PhoneType", QJsonValue(QLatin1String("Android")));
	reply.insert("SoftwareRelease", QJsonValue(QLatin1String("4.4.2")));

	_toq->sendReply(msg, 1, reply);
}
