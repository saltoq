#ifndef COMMMANAGER_H
#define COMMMANAGER_H

#include "storagemanager.h"
#include <CommHistory/CallModel>
#include <CommHistory/GroupModel>

class ContactsManager;

class CommManager : public QObject
{
	Q_OBJECT
public:
	explicit CommManager(Settings *settings, StorageManager *storage, ContactsManager *contacts, ToqManager *toq);

public slots:
	void scheduleRefresh();

signals:

private slots:
	void refresh();

private:
	ToqManager *_toq;
	ContactsManager *_contacts;
	StorageManager *_storage;
	Settings *_settings;

	CommHistory::CallModel *_calls;
	CommHistory::GroupModel *_convs;

	QTimer *_refreshTimer;
};

#endif // COMMMANAGER_H
