#ifndef STORAGEMANAGER_H
#define STORAGEMANAGER_H

#include "toqmanager.h"

class StorageManager : public QObject, public ToqManager::EndpointHandler
{
	Q_OBJECT
public:
	explicit StorageManager(ObexConnection *obex, ToqManager *toq);

	void handleMessage(const ToqConnection::Message &msg) Q_DECL_OVERRIDE;

	void updateStore(const QString &store, const QJsonObject &json);

private:
	void handleGetStoreStatusMessage(const ToqConnection::Message &msg);
	void handleGetStoreMessage(const ToqConnection::Message &msg);

	struct Store {
		QByteArray contents;
		quint32 checksum;
	};

private slots:
	void handleObexFinished();
	void handleObexError(int resp);

private:
	ObexConnection *_obex;
	ToqManager *_toq;
	QMap<QString, Store> _stores;
};

#endif // STORAGEMANAGER_H
