#ifndef CARDMANAGERADAPTOR_H
#define CARDMANAGERADAPTOR_H

#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusAbstractAdaptor>
#include <QtDBus/QDBusMessage>
#include <QtDBus/QDBusObjectPath>

#include "cardmanager.h"

class CardManagerAdaptor : public QDBusAbstractAdaptor
{
	Q_OBJECT
	Q_CLASSINFO("D-Bus Interface", "com.javispedro.saltoq.CardManager")

public:
	explicit CardManagerAdaptor(CardManager *parent = 0);

signals:

public slots:
	QDBusObjectPath CreateDeck(const QString &application, const QDBusMessage &msg);

private:
	QDBusConnection _conn;
	CardManager *_mgr;
	QHash<QDBusObjectPath, CardDeck*> _decks;
};

#endif // CARDMANAGERADAPTOR_H
