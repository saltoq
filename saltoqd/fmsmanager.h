#ifndef FMSMANAGER_H
#define FMSMANAGER_H

#include <QtCore/QDateTime>
#include "toqmanager.h"

class ObexTransfer;

class FmsManager : public QObject, public ToqManager::EndpointHandler
{
	Q_OBJECT
public:
	explicit FmsManager(ObexConnection *obex, ToqManager *toq);

	void handleMessage(const ToqConnection::Message &msg) Q_DECL_OVERRIDE;

	void updateFile(const QString &path, const QByteArray &contents);
	void deleteFile(const QString &path);

private:
	void handleTransferResult(const QJsonObject &msg);
	void handleDeleteResult(const QJsonObject &msg);

private slots:
	void handleQueue();
	void handleToqDisconnected();
	void handleObexConnected();
	void handleObexDisconnected();
	void handleObexError(int response);
	void cleanCurTransaction();

private:
	struct File {
		QDateTime mtime;
		QByteArray contents;
		quint32 checksum;
	};

private:
	ObexConnection *_obex;
	ToqManager *_toq;
	QMap<QString, File> _files;
	QSet<QString> _pending;
	int _curTransaction;
	ObexTransfer *_curTransfer;
	QString _curPath;
};

#endif // FMSMANAGER_H
