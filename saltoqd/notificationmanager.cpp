#include <QtCore/QFileInfo>
#include "notificationmanager.h"
#include "../libwatchfish/notification.h"
#include "../libwatchfish/notificationmonitor.h"

using namespace watchfish;

static NotificationMonitor *monitor = 0;

NotificationManager::NotificationManager(CardManager *card, ToqManager *toq) :
	QObject(toq), _toq(toq),
	_card(card),
	_deck(new CardDeck("com.qualcomm.qce.androidnotifications", "Notifications", this))
{
	_card->installDeck(_deck);

	if (monitor == 0) {
		monitor = new NotificationMonitor;
	}

	connect(monitor, &NotificationMonitor::notification,
			this, &NotificationManager::handleNotification);
}

void NotificationManager::handleNotification(Notification *n)
{
	uint notificationId = n->id();

	if (n->sender().isEmpty() || (n->body().isEmpty() && n->summary().isEmpty())) {
		// Never create a card for an empty notification
		qDebug() << "Ignoring empty notification" << notificationId;
		return;
	}

	if (n->transient()) {
		qDebug() << "Ignoring transient notification" << notificationId;
		return;
	}

	Card *card = new Card(QString::number(qint64(notificationId)));

	card->setHeader(n->sender());
	card->setTitle(n->summary());
	card->setText(n->body());
	card->setDateTime(n->timestamp());
	card->setVibrate(true);

	if (!n->icon().isEmpty()) {
		QFileInfo imgFile(n->icon());
		QImage img(imgFile.absoluteFilePath());
		if (!img.isNull()) {
			card->setIcon(_card->sendImage(_deck, imgFile.completeBaseName(), img.scaled(48, 48, Qt::KeepAspectRatio)));
		} else {
			qDebug() << "Could not read icon from notification: " << imgFile.absoluteFilePath();
		}
	}

	connect(n, &Notification::bodyChanged,
			this, &NotificationManager::handleNotificationBodyChanged);
	connect(n, &Notification::closed,
			this, &NotificationManager::handleClosedNotification);

	_cards.insert(notificationId, card);
	_deck->insertCard(0, card);
}

void NotificationManager::handleNotificationBodyChanged()
{
	Notification *n = static_cast<Notification*>(sender());
	uint notificationId = n->id();
	Card *card = _cards.take(notificationId);
	if (card) {
		card->setText(n->body());
	} else {
		qDebug() << "Notification" << notificationId << "does not have an attached card";
	}
}

void NotificationManager::handleClosedNotification()
{
	Notification *n = static_cast<Notification*>(sender());
	uint notificationId = n->id();
	Card *card = _cards.take(notificationId);
	if (card) {
		_deck->removeCard(card);
		card->deleteLater();
	} else {
		qDebug() << "Notification" << notificationId << "does not have an attached card";
	}
	disconnect(n, 0, this, 0);
}
