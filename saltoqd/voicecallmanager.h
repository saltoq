#ifndef VOICECALLMANAGER_H
#define VOICECALLMANAGER_H

#include "toqmanager.h"

struct ProfileValue {
	QString key;
	QString val;
	QString type;
};

class ContactsManager;
class OrgNemomobileVoicecallVoiceCallInterface;

class VoiceCallManager : public QObject, public ToqManager::EndpointHandler
{
	Q_OBJECT
public:
	explicit VoiceCallManager(ContactsManager *contacts, ToqManager *toq);

	void handleMessage(const ToqConnection::Message &msg) Q_DECL_OVERRIDE;

	static void setSilentMode(bool silent);

private:
	void sendReply(const ToqConnection::Message &msg, int status, const QString &message);

	static QString getCurrentProfile();
	static void setProfile(const QString &name);

	QJsonObject buildPhoneStatus();
	void sendPhoneStatusMessage();
	void sendPhoneRingMessage();

private slots:
	void handleProfileChanged(bool changed, bool active, const QString &profile);
	void handleActiveVoiceCallChanged();
	void handleActiveVoiceCallStatusChanged();

private:
	ToqManager *_toq;
	ContactsManager *_contacts;
	OrgNemomobileVoicecallVoiceCallInterface *_activeCall;
};

class QDBusArgument;
QDBusArgument &operator<<(QDBusArgument &argument, const ProfileValue &value);
const QDBusArgument &operator>>(const QDBusArgument &argument, ProfileValue &value);

Q_DECLARE_METATYPE(ProfileValue)
Q_DECLARE_METATYPE(QList<ProfileValue>)

#endif // VOICECALLMANAGER_H
