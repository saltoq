#ifndef CARDMANAGER_H
#define CARDMANAGER_H

#include <QtGui/QImage>
#include "fmsmanager.h"

class CardManager;
class CardDeck;
class Card;

class Card : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QString id READ id CONSTANT)
	Q_PROPERTY(QString header READ header WRITE setHeader NOTIFY headerChanged)
	Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
	Q_PROPERTY(QString info READ info WRITE setInfo NOTIFY infoChanged)
	Q_PROPERTY(QDateTime dateTime READ dateTime WRITE setDateTime NOTIFY dateTimeChanged)
	Q_PROPERTY(QString icon READ icon WRITE setIcon NOTIFY iconChanged)
	Q_PROPERTY(QString picture READ picture WRITE setPicture NOTIFY pictureChanged)
	Q_PROPERTY(bool vibrate READ isVibrate WRITE setVibrate NOTIFY vibrateChanged)
	Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
	Q_PROPERTY(QStringList menuOptions READ menuOptions WRITE setMenuOptions NOTIFY menuOptionsChanged)
	// TODO card events, divider
	Q_PROPERTY(bool open READ isOpen NOTIFY openChanged)
	Q_PROPERTY(bool visible READ isVisible NOTIFY visibleChanged)

public:
	explicit Card(const QString &id, QObject *parent = 0);

	QString id() const;

	QString header() const;
	void setHeader(const QString &header);

	QString title() const;
	void setTitle(const QString &title);

	QString info() const;
	void setInfo(const QString &info);

	QDateTime dateTime() const;
	void setDateTime(const QDateTime &dt);

	QString icon() const;
	void setIcon(const QString &url);

	QString picture() const;
	void setPicture(const QString &url);

	bool isVibrate() const;
	void setVibrate(bool vibrate);

	QString text() const;
	void setText(const QString &text);

	QStringList menuOptions() const;
	void setMenuOptions(const QStringList &options);

	bool isOpen() const;
	void setOpen(bool open);

	bool isVisible() const;
	void setVisible(bool visible);

signals:
	void headerChanged();
	void titleChanged();
	void infoChanged();
	void dateTimeChanged();
	void iconChanged();
	void pictureChanged();
	void vibrateChanged();
	void textChanged();
	void menuOptionsChanged();
	void openChanged();
	void visibleChanged();

	void optionSelected(const QString &option);

private:
	QString _id;
	QString _header;
	QString _title;
	QString _info;
	QDateTime _dateTime;
	QString _icon;
	QString _picture;
	bool _vibrate;
	QString _text;
	QStringList _options;
	bool _open;
	bool _visible;
};

class CardDeck : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QString package READ package CONSTANT)
	Q_PROPERTY(QString application READ application CONSTANT)
	Q_PROPERTY(QList<Card*> cards READ cards)

public:
	explicit CardDeck(const QString &package, const QString &application, QObject *parent = 0);

	QString package() const;
	QString application() const;

	QList<Card*> cards() const;
	Card * cardAt(int position) const;
	Card * cardAt(const QString &id) const;

	void appendCard(Card * card);
	void insertCard(int position, Card * card);
	void removeCard(int position);
	void removeCard(Card * card);

signals:
	void messageReceived(const QString &message);
	void needsRefresh();
	void cardAdded(Card * card);
	void cardRemoved(Card * card);

private slots:
	void scheduleRefresh();

private:
	QString _package;
	QString _application;
	QList<Card*> _deck;
	QHash<QString, Card*> _ids;
	QTimer *_refreshTimer;
};

class CardManager : public QObject, public ToqManager::EndpointHandler
{
	Q_OBJECT
public:
	explicit CardManager(FmsManager *fms, ToqManager *toq);

	void handleMessage(const ToqConnection::Message &msg) Q_DECL_OVERRIDE;

public slots:
	void installDeck(CardDeck *deck);
	void uninstallDeck(CardDeck *deck);

	QString sendImage(CardDeck *deck, const QString &iconName, const QImage &image);

private:
	static QString escapeString(const QString &s);
	QString generateCardDescription(const QString &verb, Card * card) const;
	QByteArray packMessage(CardDeck *deck, const QString &msg) const;
	QPair<CardDeck *, QString> unpackMessage(const QByteArray &data) const;
	QHash<QString,QString> unpackDictionary(const QString &data) const;

private slots:
	void refreshDeck(CardDeck *deck);
	void handleToqConnected();
	void handleDeckNeedsRefresh();
	void handleCardAdded(Card * card);
	void handleCardRemoved(Card * card);

private:
	ToqManager *_toq;
	FmsManager *_fms;
	QHash<QString, CardDeck*> _decks;
	QSet<QString> _pending;
};

#endif // CARDMANAGER_H
