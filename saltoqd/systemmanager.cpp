#include <time.h>
#include "systemmanager.h"
#include "voicecallmanager.h"

SystemManager::SystemManager(ToqManager *toq) :
	QObject(toq), _toq(toq)
{
	_toq->setEndpointListener(ToqConnection::SystemEndpoint, this);
}

void SystemManager::handleMessage(const ToqConnection::Message &msg)
{
	Q_ASSERT(msg.destination == ToqConnection::SystemEndpoint);
	switch (msg.type) {
	case 0:
		handleGetTimeMessage(msg);
		break;
	case 7:
		handleSilenceMessage(msg);
		break;
	default:
		qWarning() << "Unknown system message" << msg.type;
		break;
	}
}

void SystemManager::handleGetTimeMessage(const ToqConnection::Message &msg)
{
	QJsonObject reply, detail;

	// Seems that QTimeZone is completely broken on Jolla ("timed/localtime")
	time_t now;
	tm now_info;
	time(&now);
	localtime_r(&now, &now_info);

	int offset = now_info.tm_gmtoff;
	// Seems that the watch manually adds 1h if we are on DST.
	if (now_info.tm_isdst) offset -= 3600;

	detail.insert("epoch_time", qint64(now));
	detail.insert("time_zone", offset);
	detail.insert("dst", int(now_info.tm_isdst));

	reply.insert("result", int(0));
	reply.insert("description", QLatin1String("current time"));
	reply.insert("time", detail);

	_toq->sendReply(msg, 0x4000, reply);
}

void SystemManager::handleSilenceMessage(const ToqConnection::Message &msg)
{
	QJsonObject obj = msg.toJson().object();
	QJsonObject reply;

	reply.insert("result", int(0));
	reply.insert("description", QLatin1String("Set to Silence Mode Request received"));

	VoiceCallManager::setSilentMode(obj["silence_mode"].toInt());

	_toq->sendReply(msg, 0x4007, reply);
}
