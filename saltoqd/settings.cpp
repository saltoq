#include "settings.h"

Settings::Settings(const QString &path, QObject *parent) :
	MDConfGroup(path, parent, BindProperties)
{
	resolveMetaObject();
}

void Settings::setAddress(const QString &addr)
{
	if (addr != _address) {
		_address = addr;
		emit addressChanged();
	}
}
