#include <QtCore/QJsonArray>
#include "agendamanager.h"

const int LIMIT_EVENTS = 10;
const int LIMIT_DAYS = 7 * 2;

AgendaManager::AgendaManager(StorageManager *storage, ToqManager *toq) :
	QObject(toq), _toq(toq), _storage(storage),
	_calendar(new mKCal::ExtendedCalendar(KDateTime::Spec::LocalZone())),
	_calendarStorage(_calendar->defaultStorage(_calendar)),
	_refreshTimer(new QTimer(this))
{
	_refreshTimer->setSingleShot(true);
	_refreshTimer->setInterval(2000);
	connect(_refreshTimer, &QTimer::timeout,
			this, &AgendaManager::refresh);

	_calendarStorage->registerObserver(this);

	if (_calendarStorage->open()) {
		scheduleRefresh();
	} else {
		qWarning() << "Cannot open calendar database";
	}
}

AgendaManager::~AgendaManager()
{
	_calendarStorage->unregisterObserver(this);
}

void AgendaManager::scheduleRefresh()
{
	if (!_refreshTimer->isActive()) {
		_refreshTimer->start();
	}
}

void AgendaManager::refresh()
{
	qDebug() << "Now refreshing";

	QDate today = QDate::currentDate();
	QDate endDate = today.addDays(LIMIT_DAYS);
	int count = 0;
	count += _calendarStorage->loadRecurringIncidences();
	qDebug() << "Loaded" << count << "recurring events";
	count += _calendarStorage->load(today, endDate);
	qDebug() << "Loaded" << count << "events total";

	QMap<QDate, QJsonArray> recordsByDay;

	auto events = _calendar->rawExpandedEvents(today, endDate, true, true);
	for (const auto &expanded : events) {
		const QDateTime &start = expanded.first.dtStart;
		const QDateTime &end = expanded.first.dtEnd;
		KCalCore::Incidence::Ptr incidence = expanded.second;
		QJsonObject record;
		QJsonObject details;
		details.insert("StartTime", qint64(start.toTime_t()));
		details.insert("EndTime", qint64(end.toTime_t()));
		details.insert("AllDay", incidence->allDay());
		details.insert("Title", incidence->summary());
		if (!incidence->location().isEmpty()) {
			details.insert("Location", incidence->location());
		}
		mKCal::Notebook::Ptr notebook = _calendarStorage->notebook(_calendar->notebook(incidence));
		if (notebook) {
			details.insert("Organizer", notebook->name());
		}
		record.insert("ItemId", incidence->uid());
		record.insert("AgendaDetails", details);

		recordsByDay[start.date()].append(record);
	}

	QJsonArray dayRecords;
	for (auto it = recordsByDay.begin(); it != recordsByDay.end(); ++it) {
		QDateTime dt(it.key(), QTime(0, 0, 0));
		QJsonObject record;
		QJsonObject payload;
		payload.insert("Date", qint64(dt.toTime_t()));
		payload.insert("AgendaRecords", it.value());
		record.insert("RecordId", dayRecords.size() + 1);
		record.insert("RecordPayload", payload);
		dayRecords.append(record);
	}

	QString storeName("Phub.Phone.Agenda");
	QJsonObject store;
	store.insert("Name", storeName);
	store.insert("Records", dayRecords);

	QJsonObject root;
	root.insert("DataStore", store);

	_storage->updateStore(storeName, root);
}

void AgendaManager::storageModified(mKCal::ExtendedStorage *storage, const QString &info)
{
	Q_UNUSED(storage);
	qDebug() << "Storage modified:" << info;
	scheduleRefresh();
}

void AgendaManager::storageProgress(mKCal::ExtendedStorage *storage, const QString &info)
{
	Q_UNUSED(storage);
	Q_UNUSED(info);
	// Nothing to do
}

void AgendaManager::storageFinished(mKCal::ExtendedStorage *storage, bool error, const QString &info)
{
	Q_UNUSED(storage);
	Q_UNUSED(error);
	Q_UNUSED(info);
	// Nothing to do
}
