#ifndef MSOLIMAGEIOHANDLER_H
#define MSOLIMAGEIOHANDLER_H

#include <QtGui/QImageIOHandler>

class MSOLImageIOHandler : public QImageIOHandler
{
public:
	MSOLImageIOHandler();

	bool canRead() const Q_DECL_OVERRIDE;
	bool read(QImage *image) Q_DECL_OVERRIDE;
	bool write(const QImage &image) Q_DECL_OVERRIDE;

	bool supportsOption(ImageOption option) const Q_DECL_OVERRIDE;
	QVariant option(ImageOption option) const Q_DECL_OVERRIDE;
	void setOption(ImageOption option, const QVariant &value) Q_DECL_OVERRIDE;
};

QByteArray convertImageToMsol(const QImage &img);
QImage convertMsolToImage(const QByteArray &msol);
QImage ditherForMsol(const QImage &src);

#endif // MSOLIMAGEIOHANDLER_H
