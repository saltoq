#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H

#include "toqmanager.h"

class SettingsManager : public QObject
{
	Q_OBJECT
public:
	explicit SettingsManager(Settings *settings, FmsManager *fms, ToqManager *toq);

public slots:
	void sync();

private:
	ToqManager *_toq;
	FmsManager *_fms;
	Settings *_settings;
};

#endif // SETTINGSMANAGER_H
