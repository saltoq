TEMPLATE = app
CONFIG += console
QT -= qml
QT += dbus bluetooth contacts

CONFIG += c++11 link_pkgconfig

PKGCONFIG += qt5-boostable zlib dbus-1 mlite5 commhistory-qt5 openobex qtcontacts-sqlite-qt5-extensions libmkcal-qt5 libkcalcoren-qt5
INCLUDEPATH += /usr/include/mlite5 /usr/include/commhistory-qt5 /usr/include/mkcal-qt5 /usr/include/kcalcoren-qt5

INCLUDEPATH += $$PWD/../libwatchfish
DEPENDPATH += $$PWD/../libwatchfish
LIBS += -L$$OUT_PWD/../libwatchfish/ -lwatchfish

SOURCES += main.cpp \
    toqconnection.cpp \
    toqmanager.cpp \
    versionmanager.cpp \
    systemmanager.cpp \
    musicmanager.cpp \
    storagemanager.cpp \
    commmanager.cpp \
    notificationmanager.cpp \
    voicecallmanager.cpp \
    fmsmanager.cpp \
    weathermanager.cpp \
    obexconnection.cpp \
    contactsmanager.cpp \
    cardmanager.cpp \
    agendamanager.cpp \
    settings.cpp \
    settingsmanager.cpp \
    toqmanageradaptor.cpp \
    msolimageiohandler.cpp \
    cardmanageradaptor.cpp

HEADERS += \
    toqconnection.h \
    toqmanager.h \
    versionmanager.h \
    systemmanager.h \
    musicmanager.h \
    storagemanager.h \
    commmanager.h \
    notificationmanager.h \
    voicecallmanager.h \
    fmsmanager.h \
    weathermanager.h \
    obexconnection.h \
    contactsmanager.h \
    cardmanager.h \
    agendamanager.h \
    settings.h \
    settingsmanager.h \
    toqmanageradaptor.h \
    msolimageiohandler.h \
    cardmanageradaptor.h \
    global.h

DBUS_INTERFACES += com.nokia.profiled.xml org.nemomobile.voicecall.VoiceCallManager.xml org.nemomobile.voicecall.VoiceCall.xml
QDBUSXML2CPP_INTERFACE_HEADER_FLAGS = -i voicecallmanager.h

target.path = /usr/bin
INSTALLS += target

res.path = /usr/share/saltoq/res
res.files = res/Q-Bold.otf res/Q-Light.otf res/Q-Regular.otf res/Q-Semibold.otf res/*.png
INSTALLS += res

unit.path = /usr/lib/systemd/user
unit.files = saltoqd.service
INSTALLS += unit

workaround.path = /usr/lib/systemd/system
workaround.files = saltoq-bt-sniff-subrate.service
INSTALLS += workaround
