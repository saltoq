#ifndef CONTACTSMANAGER_H
#define CONTACTSMANAGER_H

#include <QtContacts/QContactManager>
#include <QtContacts/QContactPhoneNumber>
#include "storagemanager.h"

class ContactsManager : public QObject
{
	Q_OBJECT
public:
	explicit ContactsManager(StorageManager *storage, ToqManager *toq);

	qint64 getRecordIdForContact(uint contactId);

	struct NameType {
		bool found;
		QString name;
		QString type;
		bool favorite;
	};

	NameType findNameTypeForPhoneNumber(const QString &phoneNumber) const;

public slots:
	void scheduleRefresh();

signals:
	void changed();

private slots:
	void refresh();

private:
	static QString typeForContactPhone(const QtContacts::QContactPhoneNumber &number);

private:
	ToqManager *_toq;
	StorageManager *_storage;
	QtContacts::QContactManager *_contacts;
	QTimer *_refreshTimer;
};

#endif // CONTACTSMANAGER_H
