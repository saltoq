#ifndef OBEXCONNECTION_H
#define OBEXCONNECTION_H

#include <QtCore/QQueue>
#include "toqconnection.h"

typedef void* obex_t;
typedef void* obex_object_t;

class ObexConnection;

class ObexTransfer : public QObject
{
	Q_OBJECT

	friend class ObexConnection;

	explicit ObexTransfer(obex_t *obex, obex_object_t *obj, QObject *parent);

public slots:
	void cancel();

signals:
	void finished();
	void error(int response);

private:
	obex_t *_obex;
	obex_object_t *_obj;
};

class ObexConnection : public QObject
{
	Q_OBJECT
	Q_PROPERTY(bool connected READ isConnected)

public:
	explicit ObexConnection(ToqConnection *conn, QObject *parent = 0);

	bool isConnected() const;

	ObexTransfer *put(const QString &name, const QByteArray &data);

signals:
	void connected();
	void disconnected();

private slots:
	void tryConnect();
	void handleToqConnected();
	void handleToqDisconnected();
	void handleSocketConnected();
	void handleSocketDisconnected();
	void handleSocketError(QBluetoothSocket::SocketError error);
	void handleSocketData();
	void handleTransferDestroyed();
	void handleNextPending();

private:
	static void handleObexEvent(obex_t *handle, obex_object_t *obj, int mode, int event, int obex_cmd, int obex_rsp);
	static int obexConnect(obex_t *handle, void *data);
	static int obexDisconnect(obex_t *handle, void *data);
	static int obexListen(obex_t *handle, void *data);
	static int obexWrite(obex_t *handle, void *data, quint8 *buf, int buflen);
	static int obexHandleInput(obex_t *handle, void *data, int timeout);

	ToqConnection *_conn;
	QBluetoothSocket *_socket;
	QTimer *_reconnectTimer;
	obex_t *_obex;
	bool _connected;

	QQueue<ObexTransfer*> _pending;
	bool _busy;
};

inline bool ObexConnection::isConnected() const
{
	return _connected;
}

#endif // OBEXCONNECTION_H
