#ifndef TOQMANAGERADAPTOR_H
#define TOQMANAGERADAPTOR_H

#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusAbstractAdaptor>
#include <QtDBus/QDBusMessage>

#include "toqmanager.h"

class ToqManagerAdaptor : public QDBusAbstractAdaptor
{
	Q_OBJECT
	Q_CLASSINFO("D-Bus Interface", "com.javispedro.saltoq.ToqManager")

	Q_PROPERTY(bool connected READ isConnected NOTIFY connectedChanged)

public:
	ToqManagerAdaptor(FmsManager *fms, ToqManager *parent);

	bool isConnected() const;

public slots:
	void PutData(const QByteArray &data, const QString &remoteFile);
	void PutFile(const QString &localFile, const QString &remoteFile, const QDBusMessage &msg);
	void PutImage(const QString &localFile, const QString &remoteFile, const QDBusMessage &msg);

signals:
	void connectedChanged();

private:
	QDBusConnection _conn;
	ToqManager *_toq;
	FmsManager *_fms;
};

inline bool ToqManagerAdaptor::isConnected() const
{
	return _toq->isConnected();
}

#endif // TOQMANAGERADAPTOR_H
