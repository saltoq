#include "cardmanageradaptor.h"

CardManagerAdaptor::CardManagerAdaptor(CardManager *parent)
	: QDBusAbstractAdaptor(parent),
	  _conn(QDBusConnection::sessionBus()),
	  _mgr(parent)
{
	setAutoRelaySignals(true);
}

QDBusObjectPath CardManagerAdaptor::CreateDeck(const QString &application, const QDBusMessage &msg)
{
	QString sender = msg.service();
	CardDeck *deck = new CardDeck(sender, application, _mgr);
	QDBusObjectPath path("/com/javispedro/saltoq/CardManager/" + application);
	deck->setObjectName(application);
	_mgr->installDeck(deck);
	_decks.insert(path, deck);
	return path;
}
