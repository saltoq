#include <QtGui/QGuiApplication>
#include <QtGui/QFontDatabase>
#include <QtDBus/QDBusConnection>
#include "global.h"
#include "toqmanager.h"

Q_DECL_EXPORT int main(int argc, char **argv)
{
	QGuiApplication app(argc, argv);

	QFontDatabase::addApplicationFont(RES_PATH "/Q-Light.otf");
	int id = QFontDatabase::addApplicationFont(RES_PATH "/Q-Regular.otf");
	qDebug() << QFontDatabase::applicationFontFamilies(id);
	QFontDatabase::addApplicationFont(RES_PATH "/Q-Semibold.otf");
	QFontDatabase::addApplicationFont(RES_PATH "/Q-Bold.otf");

	QScopedPointer<Settings> settings(new Settings("/apps/saltoq"));
	QScopedPointer<ToqManager> manager(new ToqManager(settings.data()));

	QDBusConnection::sessionBus().registerService("com.javispedro.saltoq.daemon");

	return app.exec();
}
