#ifndef NOTIFICATIONMANAGER_H
#define NOTIFICATIONMANAGER_H

#include "cardmanager.h"

namespace watchfish
{
class Notification;
class NotificationMonitor;
}

class NotificationManager : public QObject
{
	Q_OBJECT

public:
	explicit NotificationManager(CardManager *card, ToqManager *toq);

private slots:
	void handleNotification(watchfish::Notification *n);
	void handleNotificationBodyChanged();
	void handleClosedNotification();

private:
	ToqManager *_toq;
	CardManager *_card;
	CardDeck *_deck;
	QMap<uint, Card*> _cards;
};

#endif // NOTIFICATIONMANAGER_H
