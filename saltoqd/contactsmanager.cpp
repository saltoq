#include "contactsmanager.h"
#include <QtContacts/QtContacts>
#include <qtcontacts-extensions_impl.h>

QTCONTACTS_USE_NAMESPACE

ContactsManager::ContactsManager(StorageManager *storage, ToqManager *toq) :
	QObject(toq), _toq(toq), _storage(storage),
	_contacts(new QContactManager(this)),
	_refreshTimer(new QTimer(this))
{
	connect(_contacts, &QContactManager::contactsAdded,
			this, &ContactsManager::scheduleRefresh);
	connect(_contacts, &QContactManager::dataChanged,
			this, &ContactsManager::scheduleRefresh);

	connect(_refreshTimer, &QTimer::timeout,
			this, &ContactsManager::refresh);

	_refreshTimer->setSingleShot(true);
	_refreshTimer->setInterval(2000);
	_refreshTimer->start();
}

qint64 ContactsManager::getRecordIdForContact(uint contactId)
{
	return contactId;
}

ContactsManager::NameType ContactsManager::findNameTypeForPhoneNumber(const QString &phoneNumber) const
{
	NameType result;

	result.found = false;
	result.favorite = false;

	if (!phoneNumber.isEmpty()) {
		const int maxCharacters = QtContactsSqliteExtensions::DefaultMaximumPhoneNumberCharacters;
		QString normPhoneNumber = QtContactsSqliteExtensions::minimizePhoneNumber(phoneNumber, maxCharacters);
		QList<QContact> contacts = _contacts->contacts(QContactPhoneNumber::match(normPhoneNumber));

		if (!contacts.isEmpty()) {
			const QContact& contact = contacts.first();
			result.found = true;
			result.name = contact.detail<QContactDisplayLabel>().label();
			result.favorite = contact.detail<QContactFavorite>().isFavorite();

			for (const QContactPhoneNumber &phone : contact.details<QContactPhoneNumber>()) {
				if (phone.number() == normPhoneNumber) {
					result.type = typeForContactPhone(phone);
					break;
				}
			}
		} else {
			result.found = false;
		}
	}

	if (result.name.isEmpty()) {
		if (phoneNumber.isEmpty()) {
			result.name = tr("Unknown");
		} else {
			result.name = phoneNumber;
		}
	}
	if (result.type.isEmpty()) {
		result.type = "Other";
	}

	return result;
}

void ContactsManager::scheduleRefresh()
{
	if (!_refreshTimer->isActive()) {
		_refreshTimer->start();
	}
}

void ContactsManager::refresh()
{
	qDebug() << "Refreshing contacts";

	QContactSortOrder order;
	order.setBlankPolicy(QContactSortOrder::BlanksLast);
	order.setCaseSensitivity(Qt::CaseInsensitive);
	order.setDetailType(QContactDetail::TypeDisplayLabel, QContactDisplayLabel::FieldLabel);

	QList<QContact> contacts = _contacts->contacts(order);

	QJsonArray records;
	for (const QContact &contact : contacts) {
		QJsonObject payload;
		QContactName cName = contact.detail<QContactName>();
		QContactDisplayLabel cDisplay = contact.detail<QContactDisplayLabel>();
		QContactFavorite cFav = contact.detail<QContactFavorite>();
		QJsonObject name;
		name.insert("Initial", cName.prefix());
		name.insert("First", cName.firstName());
		name.insert("Middle", cName.middleName());
		name.insert("Last", cName.lastName());
		name.insert("Display", cDisplay.label());
		QJsonArray phones;
		for (const QContactPhoneNumber &cPhone : contact.details<QContactPhoneNumber>()) {
			QJsonObject phone;
			phone.insert("Type", typeForContactPhone(cPhone));
			phone.insert("Number", cPhone.number());
			phones.append(phone);
		}
		payload.insert("Name", name);
		payload.insert("PhoneNumber", phones);
		payload.insert("IsFav", cFav.isFavorite());
		QJsonObject record;
		record.insert("RecordId", getRecordIdForContact(QtContactsSqliteExtensions::internalContactId(contact.id())));
		record.insert("RecordPayload", payload);
		records.append(record);
	}

	QString storeName("Phub.Phone.Contacts");
	QJsonObject store;
	store.insert("Name", storeName);
	store.insert("Records", records);

	QJsonObject root;
	root.insert("DataStore", store);

	qDebug() << "Got" << records.size() << "records";

	_storage->updateStore(storeName, root);

	emit changed();
}

QString ContactsManager::typeForContactPhone(const QContactPhoneNumber &number)
{
	QString type;
	if (!number.subTypes().isEmpty()) {
		if (number.subTypes().first() == QContactPhoneNumber::SubTypeMobile) {
			type = "Mobile";
		}
	}
	if (type.isEmpty() && !number.contexts().isEmpty()) {
		switch (number.contexts().first()) {
		case QContactDetail::ContextHome:
			type = "Home";
			break;
		case QContactDetail::ContextWork:
			type = "Work";
			break;
		default:
			break;
		}
	}
	if (type.isEmpty()) {
		type = "Other";
	}
	return type;
}
