#ifndef SYSTEMMANAGER_H
#define SYSTEMMANAGER_H

#include "toqmanager.h"

class SystemManager : public QObject, public ToqManager::EndpointHandler
{
	Q_OBJECT
public:
	explicit SystemManager(ToqManager *toq);

	void handleMessage(const ToqConnection::Message &msg) Q_DECL_OVERRIDE;

private:
	void handleGetTimeMessage(const ToqConnection::Message &msg);
	void handleSilenceMessage(const ToqConnection::Message &msg);

private:
	ToqManager *_toq;
};

#endif // SYSTEMMANAGER_H
