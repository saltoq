#ifndef SETTINGS_H
#define SETTINGS_H

#include <MDConfGroup>
#include "global.h"

class Settings : public MDConfGroup
{
	Q_OBJECT
	Q_PROPERTY(QString address READ address WRITE setAddress NOTIFY addressChanged)

public:
	explicit Settings(const QString &path, QObject *parent = 0);

	QString address() const;
	void setAddress(const QString &addr);

signals:
	void addressChanged();

private:
	QString _address;
};

inline QString Settings::address() const
{
	return _address;
}

#endif // SETTINGS_H
