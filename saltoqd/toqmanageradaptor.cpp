#include <QtCore/QFile>
#include <QtGui/QImage>

#include "toqmanageradaptor.h"
#include "msolimageiohandler.h"
#include "fmsmanager.h"

ToqManagerAdaptor::ToqManagerAdaptor(FmsManager *fms, ToqManager *parent)
	: QDBusAbstractAdaptor(parent),
	  _conn(QDBusConnection::sessionBus()),
	  _toq(parent), _fms(fms)
{
	setAutoRelaySignals(true);
}

void ToqManagerAdaptor::PutData(const QByteArray &data, const QString &remoteFile)
{
	_fms->updateFile(remoteFile, data);
}

void ToqManagerAdaptor::PutFile(const QString &localFile, const QString &remoteFile, const QDBusMessage &msg)
{
	QFile f(localFile);
	if (!f.open(QIODevice::ReadOnly)) {
		QDBusMessage reply = msg.createErrorReply("com.javispedro.saltoq.InvalidFile", "File does not exist");
		_conn.send(reply);
		return;
	}

	_fms->updateFile(remoteFile, f.readAll());
}

void ToqManagerAdaptor::PutImage(const QString &localFile, const QString &remoteFile, const QDBusMessage &msg)
{
	QFile f(localFile);
	if (!f.exists()) {
		QDBusMessage reply = msg.createErrorReply("com.javispedro.saltoq.InvalidFile", "File does not exist");
		_conn.send(reply);
		return;
	}
	QImage image(localFile);
	if (image.isNull())  {
		QDBusMessage reply = msg.createErrorReply("com.javispedro.saltoq.InvalidImage", "Cannot read image");
		_conn.send(reply);
		return;
	}
	QByteArray msol = convertImageToMsol(image);
	if (msol.isEmpty()) {
		QDBusMessage reply = msg.createErrorReply("com.javispedro.saltoq.InvalidImage", "Cannot convert this image");
		_conn.send(reply);
		return;
	}

	_fms->updateFile(remoteFile, msol);
}
