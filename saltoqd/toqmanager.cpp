#include <QtDBus/QDBusConnection>

#include "toqmanager.h"
#include "toqmanageradaptor.h"

#include "obexconnection.h"

#include "versionmanager.h"
#include "systemmanager.h"
#include "fmsmanager.h"
#include "storagemanager.h"
#include "settingsmanager.h"
#include "musicmanager.h"
#include "contactsmanager.h"
#include "commmanager.h"
#include "agendamanager.h"
#include "voicecallmanager.h"
#include "weathermanager.h"
#include "notificationmanager.h"

static const bool PROTO_DEBUG = true;

ToqManager::ToqManager(Settings *settings, QObject *parent) :
	QObject(parent),
	_settings(settings),
	_conn(new ToqConnection(this)),
	_obex(new ObexConnection(_conn, this)),
	_versionManager(new VersionManager(this)),
	_systemManager(new SystemManager(this)),
	_fmsManager(new FmsManager(_obex, this)),
	_storageManager(new StorageManager(_obex, this)),
	_settingsManager(new SettingsManager(_settings, _fmsManager, this)),
	_musicManager(new MusicManager(this)),
	_contactsManager(new ContactsManager(_storageManager, this)),
	_commManager(new CommManager(_settings, _storageManager, _contactsManager, this)),
	_agendaManager(new AgendaManager(_storageManager, this)),
	_voiceCallManager(new VoiceCallManager(_contactsManager, this)),
	_weatherManager(new WeatherManager(_fmsManager, this)),
	_cardManager(new CardManager(_fmsManager, this)),
	_notificationManager(new NotificationManager(_cardManager, this))
{
	connect(_conn, &ToqConnection::messageReceived,
			this, &ToqManager::handleToqMessage);
	connect(_conn, &ToqConnection::connected,
			this, &ToqManager::connected);
	connect(_conn, &ToqConnection::disconnected,
			this, &ToqManager::disconnected);
	connect(_conn, &ToqConnection::connectedChanged,
			this, &ToqManager::connectedChanged);
	connect(_settings, &Settings::addressChanged,
			this, &ToqManager::handleSettingsAddressChanged);

	_conn->setAddress(QBluetoothAddress(_settings->address()));

	new ToqManagerAdaptor(_fmsManager, this);
	QDBusConnection::sessionBus().registerObject("/com/javispedro/saltoq/ToqManager", this);
}

void ToqManager::setEndpointListener(ToqConnection::Endpoint ep, EndpointHandler *handler)
{
	Q_ASSERT(!_handlers.contains(ep));
	_handlers.insert(ep, handler);
}

void ToqManager::sendMessage(const ToqConnection::Message &msg)
{
	if (PROTO_DEBUG) {
		QString content = QString::fromUtf8(msg.payload);
		qDebug() << "Sending message to" << ToqConnection::nameOfEndpoint(msg.destination) << "from" << ToqConnection::nameOfEndpoint(msg.source) << "type" << msg.type << content;
	}
	_conn->sendMessage(msg);
}

void ToqManager::sendMessage(ToqConnection::Endpoint source, ToqConnection::Endpoint destination, quint16 transactionId, quint32 type, const QByteArray &payload)
{
	ToqConnection::Message msg(source, destination, transactionId, type, payload);
	sendMessage(msg);
}

quint16 ToqManager::sendMessage(ToqConnection::Endpoint source, ToqConnection::Endpoint destination, quint32 type, const QByteArray &payload)
{
	quint16 transactionId = newTransactionId();
	ToqConnection::Message msg(source, destination, transactionId, type, payload);
	sendMessage(msg);
	return transactionId;
}

void ToqManager::sendMessage(ToqConnection::Endpoint source, ToqConnection::Endpoint destination, quint16 transactionId, quint32 type, const QJsonObject &payload)
{
	QJsonDocument doc(payload);
	ToqConnection::Message msg(source, destination, transactionId, type, doc);
	sendMessage(msg);
}

quint16 ToqManager::sendMessage(ToqConnection::Endpoint source, ToqConnection::Endpoint destination, quint32 type, const QJsonObject &payload)
{
	QJsonDocument doc(payload);
	quint16 transactionId = newTransactionId();
	ToqConnection::Message msg(source, destination, transactionId, type, doc);
	sendMessage(msg);
	return transactionId;
}

void ToqManager::sendReply(const ToqConnection::Message &msg, quint32 type, const QJsonObject &payload)
{
	ToqConnection::Message reply(msg.destination, msg.source, msg.transactionId, type, QJsonDocument(payload));
	sendMessage(reply);
}

void ToqManager::handleToqMessage(const ToqConnection::Message &msg)
{
	EndpointHandler *handler = _handlers.value(msg.destination, 0);

	if (PROTO_DEBUG) {
		QString content = QString::fromUtf8(msg.payload);
		qDebug() << "Received message to" << ToqConnection::nameOfEndpoint(msg.destination) << "from" << ToqConnection::nameOfEndpoint(msg.destination) << "type" << msg.type << content;
	}

	if (handler) {
		handler->handleMessage(msg);
	} else {
		qWarning() << "No registered handler for endpoint" << ToqConnection::nameOfEndpoint(msg.destination);
	}
}

void ToqManager::handleSettingsAddressChanged()
{
	_conn->setAddress(QBluetoothAddress(_settings->address()));
}
