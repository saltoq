#ifndef MUSICMANAGER_H
#define MUSICMANAGER_H

#include <QtDBus/QDBusServiceWatcher>
#include <QtDBus/QDBusContext>
#include "toqmanager.h"

class MusicManager : public QObject, public ToqManager::EndpointHandler, protected QDBusContext
{
	Q_OBJECT
public:
	explicit MusicManager(ToqManager *toq);

	void handleMessage(const ToqConnection::Message &msg) Q_DECL_OVERRIDE;

private:
	void handleGetPlayerStatusMessage(const ToqConnection::Message &msg);

	void switchToService(const QString &service);
	void fetchMetadataFromService();
	void sendCurrentMprisMetadata();
	void fillWithMetadata(QJsonObject &obj);
	void callMprisMethod(const QString &method);

private slots:
	void handleMprisServiceOwnerChanged(const QString &serviceName, const QString &oldOwner, const QString &newOwner);
	void handleMprisPropertiesChanged(const QString &interface, const QMap<QString,QVariant> &changed, const QStringList &invalidated);

private:
	ToqManager *_toq;
	QDBusServiceWatcher *_watcher;
	QString _curService;
	QVariantMap _curMetadata;
	QString _curPlaybackStatus;
	bool _isPlayerVisible;
};


#endif // MUSICMANAGER_H
